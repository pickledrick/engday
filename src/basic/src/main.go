package main

import (
	"flag"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/pickledrick/workshop/sss/src/metrics"
	"log"
	"net/http"
	"time"
)

var client = http.Client{
	Timeout: 200 * time.Millisecond,
}
var addr = flag.String("listen-address", ":9091", "The address to listen on for HTTP requests.")

func main() {

	handler := http.NewServeMux()

	handler.HandleFunc("/bbq", func(w http.ResponseWriter, r *http.Request) {

		req, _ := http.NewRequest("GET", "http://bbq", nil)

		res, err := client.Do(req)
		if res == nil {
			metrics.None.Inc()
			log.Println("SAUCELESS")
		}
		if err != nil {
			metrics.None.Inc()
			log.Println("SAUCELESS")
		}
		metrics.Bbq.Inc()
		log.Println("BBQ")
	})

	handler.Handle("/metrics", promhttp.Handler())
	withMetrics := metrics.Middleware(handler)
	log.Printf("%v", *addr)
	log.Fatal(http.ListenAndServe(*addr, withMetrics))
}
