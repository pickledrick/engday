package metrics

import (
	"fmt"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

var (
	httpRequestsResponseTime prometheus.Summary
	Bbq                      prometheus.Counter
	Tomato                   prometheus.Counter
	None                     prometheus.Counter
)

func init() {
	httpRequestsResponseTime = prometheus.NewSummary(prometheus.SummaryOpts{
		Namespace: "http",
		Name:      "response_time_seconds",
		Help:      "Request response times",
	})
	Bbq = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "bbq_fulfillments",
		Help: "Number of bbq sauce fulfillments.",
	})
	Tomato = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "tomato_fulfillments",
		Help: "Number of tomato sauce fulfillments.",
	})
	None = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "sauceless_fulfillments",
		Help: "Number of sauceless sausage sandwhiches.",
	})
	prometheus.MustRegister(httpRequestsResponseTime, Bbq, Tomato, None)
	fmt.Println("init fin")
	// prometheus.MustRegister(Bbq)
}

func Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		next.ServeHTTP(w, r)

		httpRequestsResponseTime.Observe(float64(time.Since(start).Seconds()))
	})
}
