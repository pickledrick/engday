package main

import (
	"fmt"
	"net/http"
)

func main() {
	http.HandleFunc("/", func(rw http.ResponseWriter, req *http.Request) {
		fmt.Fprintf(rw, "tomato")
	})
	fmt.Println("Error", http.ListenAndServe(":8081", nil))
}
