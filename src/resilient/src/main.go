package main

import (
	"errors"
	"flag"

	"fmt"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/pickledrick/workshop/sss/src/metrics"
	"gopkg.in/eapache/go-resiliency.v1/breaker"
	"log"
	"net/http"
	"time"
)

var client = http.Client{
	Timeout: 200 * time.Millisecond,
}
var addr = flag.String("listen-address", ":9091", "The address to listen on for HTTP requests.")

func main() {

	handler := http.NewServeMux()
	b := breaker.New(1, 3, 30*time.Second)
	handler.HandleFunc("/bbq", func(rw http.ResponseWriter, req *http.Request) {

		req, _ = http.NewRequest("GET", "http://bbq", nil)

		result := b.Run(func() error {
			res, err := client.Do(req)
			if res == nil {
				return errors.New("unable to reach bbq")
			}
			if err != nil {
				return err
			}
			return nil
		})

		switch result {
		case nil:
			metrics.Bbq.Inc()
			log.Println("BBQ")
			rw.Write([]byte("enjoy your sausage sanga with bbq\n"))
		case breaker.ErrBreakerOpen:
			req, _ = http.NewRequest("GET", "http://tomato", nil)
			res, err := client.Do(req)
			if res == nil || err != nil {
				metrics.None.Inc()
				log.Println("SAUCELESS")
				rw.Write([]byte("unable to reach fallback service\n"))
			} else {
				metrics.Tomato.Inc()
				log.Println("TOMATO")
				rw.Write([]byte("BBQ sauce not available, enjoy your sausage sanga with Tomato\n"))
			}

		default:
			fmt.Println(result)
			metrics.None.Inc()
			fmt.Println("nothing responding")
		}

	})
	handler.Handle("/metrics", promhttp.Handler())
	withMetrics := metrics.Middleware(handler)
	log.Printf("%v", *addr)
	log.Fatal(http.ListenAndServe(*addr, withMetrics))

}
